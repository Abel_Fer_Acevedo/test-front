import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/auth/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { DetailProductComponent } from './components/detail/detail-product/detail-product.component';
import { CarritoComponent } from './components/cart/carrito/carrito.component';
import { UsersComponent } from './components/custumer/users/users.component';
import { UserVipMonthComponent } from './components/custumer/user-vip-month/user-vip-month.component';
import { UsersStoppedVIPIComponent } from './components/custumer/users-stopped-vipi/users-stopped-vipi.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent},
  {path: 'product/:id', component: DetailProductComponent},
  {path: 'cart', component: CarritoComponent},
  {path: 'users', component: UsersComponent},
  {path: 'users-vip', component: UserVipMonthComponent},
  {path: 'users-stop', component: UsersStoppedVIPIComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full' },

  //{ path: '**', component: NotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
