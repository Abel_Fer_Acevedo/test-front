import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/auth/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DetailProductComponent } from './components/detail/detail-product/detail-product.component';
import { CarritoComponent } from './components/cart/carrito/carrito.component';
import { UsersComponent } from './components/custumer/users/users.component';
import { UserVipMonthComponent } from './components/custumer/user-vip-month/user-vip-month.component';
import { UsersStoppedVIPIComponent } from './components/custumer/users-stopped-vipi/users-stopped-vipi.component';


@NgModule({
  declarations: [
    AppComponent,
    AppComponent,
    LoginComponent,
    HomeComponent,
    DetailProductComponent,
    CarritoComponent,
    UsersComponent,
    UserVipMonthComponent,
    UsersStoppedVIPIComponent

  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
