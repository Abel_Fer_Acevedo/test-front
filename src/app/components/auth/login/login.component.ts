import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LoginRequest } from 'src/app/model/request/login-request';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private localAuth: LocalAuthService,
              private toastr: ToastrService,
              private router: Router,
              private cartService: CartService) {
    this.loginForm = this.fb.group({
      username: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(8)]]
    });
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  ngOnInit(): void {

  }

  login() {
    if (this.loginForm.valid) {
      const request: LoginRequest = this.loginForm.value;
      this.authService.login(request).subscribe({
        next: (response) => {
          console.log(response);
          // this.erroresDeValidacion = '';
          if (response && response.token !== "") {
            this.localAuth.setToken(response.token);
            this.localAuth.setUserId(response.userId);
            this.localAuth.setIsVip(response.isVip);
            this.localAuth.setName(response.username);
            this.toastr.success(response.message);
          }
          setTimeout(() => {
            //this.createNewTrolleyForUser();
            this.router.navigateByUrl('home');
          }, 1000);
        },
        error: (err) => {
          console.log(err)
          if(err.status === 403){
            this.toastr.error("Credenciales incorrectas");

          }
          setTimeout(() => {
            this.loginForm.reset();
          }, 2000);
        },
        complete: () => {
          console.log("Login completo con exito");
        }
      });
    } else {
      this.loginForm.markAllAsTouched();
    }
  }

  createNewTrolleyForUser(){
    const userId = this.localAuth.getUserId();
    if (userId !== null && userId !== undefined) {
      this.cartService.createNewTrolleyForUser(userId).subscribe(
        response => {
          if (response && response.id) {
            this.localAuth.setCartId(response.id);
          }
          console.log(response);
        }, err => {
          console.log(err);
        }
      )
    }
  }



}
