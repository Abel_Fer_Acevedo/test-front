import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ProductResponse } from 'src/app/model/response/product-response';
import { TrolleyResponse } from 'src/app/model/response/trolley-response';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';

import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {
  cartItemCount = 0;
  cartItemCountSubscription!: Subscription;
  countProduct = 0;
  messageStatus = '';
  subTotal = 0;
  total = 0;
  discount = '';
  userLogued: string | null = '';
  userIsVip: string | null = '';
  cart!: TrolleyResponse;
  products: ProductResponse[] = [];

  constructor(private cartService: CartService,
    private local: LocalAuthService,
    private toastr: ToastrService,
    private userService: UserService,
    private router: Router,

  ) { }


  ngOnInit(): void {
    this.messageStatus = '';
    const cartId = this.local.getCartId();
    this.createNewTrolleyForUser();
    this.userLogued = this.local.getName();
    this.userIsVip = this.local.getIsVip();
    this.getTotalCartProducts();

  }


  calculateSubTotal() {
    this.subTotal = 0;
    for (let i = 0; i < this.products.length; i++) {
      this.subTotal += this.products[i].price;
    }
  }

  createNewTrolleyForUser() {
    const userId = this.local.getUserId();
    if (userId !== null && userId !== undefined) {
      this.cartService.createNewTrolleyForUser(userId).subscribe(
        response => {

          if (response && response.id) {
            this.local.setCartId(response.id);
          }
          this.cart = response;
          this.products = response.productResponseList;
          console.log("Carrito: ", this.cart);
          console.log("Productos: ", this.products);
          this.calculateCartValue();
          this.calculateSubTotal();
        }, err => {
          console.log(err);
        }
      )
    }
  }

  calculateCartValue() {
    const cartId = this.local.getCartId();
    const cartType = this.cart.cartType;
    if (this.cart) {
      this.cartService.calculateCartValue(cartId!, cartType).subscribe(
        response => {
          console.log("Total = ", response);
          this.total = response;
          console.log("Cantidad de productos:", this.products.length);

        },
        err => {
          console.log(err);
        }
      )
    }
  }

  buyCart() {
    console.log("Se apreto el boton comprar carrito.")
    const userId = this.local.getUserId();
    if (userId != null) {
      this.cartService.buyCart(userId).subscribe(
        response => {
          console.log(response);
          this.toastr.success(response.message);
          this.deleteTrolley();
          setTimeout(() => {
            // window.location.reload();  // Recargar la página
            this.router.navigateByUrl('/home');
          }, 3000)
        },
        err => {
          console.log(err);
          this.toastr.error(err.error.message);
        }
      )
    }

  }

  deleteTrolley() {
    const cartId = this.local.getCartId();
    if (cartId != null) {
      this.cartService.deleteTrolley(cartId).subscribe(
        response => {
          console.log(response);
          this.toastr.info("Carrito eliminado");
          this.local.removeCart();
          // setTimeout(() => {
          //   window.location.reload();  // Recargar la página
          // }, 2000)

        },
        err => {
          console.log(err);
        }
      )
    }
  }

  removeProductFromCart(productId: number) {
    const cartId = this.local.getCartId();
    if (cartId != null && productId != null) {
      this.cartService.removeProductFromCart(cartId, productId).subscribe(
        response => {
          console.log(response);
          this.toastr.success(response.message);
          // Actualizar localmente la lista de productos eliminando solo el primer elemento que coincida con el productId
          const indexToRemove = this.products.findIndex(product => product.id === productId);
          if (indexToRemove !== -1) {
            this.products.splice(indexToRemove, 1);

          }
          this.calculateCartValue();
          this.calculateSubTotal();

        },
        err => {
          console.log(err);
        }
      )
    }

  }

  updateVipStatus() {
    const userId = this.local.getUserId();
    if (userId != null) {
      this.userService.updateVipStatus(userId).subscribe(
        response => {
          this.messageStatus = response.message;
          console.log(response);
          if (response.message === '¡Felicidades! Ahora eres un cliente VIP"') {
            this.local.setIsVip('true');
            const timeOutDuration = 4000;
            this.toastr.info(response.message, '', {
              timeOut: timeOutDuration,
            });
          }
          if (response.message === 'Lamentamos comunicarte que ya no eres cliente VIP') {
            this.local.setIsVip('false');
            ////modificar cart
            const timeOutDuration = 4000;
            this.toastr.info(response.message, '', {
              timeOut: timeOutDuration,
            });
          }

        },
        err => {
          console.log(err);
        }
      )
    }
  }

  getTotalCartProducts() {
    const cartId = this.local.getCartId();
    this.cartService.getTotalCartProducts(cartId!).subscribe(response => {
      this.countProduct = response;
      console.log(response);
    },
      err => {
        console.log(err);
      }
    )
  }

}
