import { Component, OnInit } from '@angular/core';
import { UsersBecameVIPInMonth } from 'src/app/model/response/users-became-vipin-month';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-vip-month',
  templateUrl: './user-vip-month.component.html',
  styleUrls: ['./user-vip-month.component.css']
})
export class UserVipMonthComponent implements OnInit{
  countProduct = 0;
  userLogued: string | null = '';
  userIsVip: string | null = '';
  usersBecameVIP: UsersBecameVIPInMonth[] = [];
  selectedMonth: number = 1;
  constructor(private userService: UserService, private local: LocalAuthService, private cartService: CartService){}

  ngOnInit(): void {
    this.getUsersBecameVIPInMonth();
    this.userLogued = this.local.getName();
    this.getTotalCartProducts();
  }

  getUsersBecameVIPInMonth(){
    this.userService.getUsersBecameVIPInMonth(this.selectedMonth).subscribe(
      response => {
        console.log("UsersBecameVIP:", response);
        this.usersBecameVIP = response;
      },
      err => {
        console.log(err);
      }
    )
  }

  getTotalCartProducts(){
    const cartId = this.local.getCartId();
    this.cartService.getTotalCartProducts(cartId!).subscribe(response => {
      this.countProduct = response;
      console.log(response);
    },
    err => {
      console.log(err);
    }
    )
  }

}
