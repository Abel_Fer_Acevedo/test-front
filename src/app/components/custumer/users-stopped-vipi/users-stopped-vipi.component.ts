import { Component, OnInit } from '@angular/core';
import { UsersStoppedBeingVIPInMonth } from 'src/app/model/response/users-stopped-being-vipin-month';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users-stopped-vipi',
  templateUrl: './users-stopped-vipi.component.html',
  styleUrls: ['./users-stopped-vipi.component.css']
})
export class UsersStoppedVIPIComponent implements OnInit{
  countProduct = 0;
  userLogued: string | null = '';
  userIsVip: string | null = '';
  selectedMonth: number = 1;
  userStopedVip: UsersStoppedBeingVIPInMonth[]=[]
  constructor(private local: LocalAuthService, private userService: UserService, private cartService: CartService){}

  ngOnInit(): void {
    this.userLogued = this.local.getName();
    this.usersStoppedBeingVIPInMonth();
    this.getTotalCartProducts();
  }

  usersStoppedBeingVIPInMonth(){
    this.userService.getUsersStoppedBeingVIPInMonth(this.selectedMonth).subscribe(
      response => {
        console.log(response);
        this.userStopedVip = response;
      },
      err => {
        console.log(err);
      }
    )
  }

  getTotalCartProducts(){
    const cartId = this.local.getCartId();
    this.cartService.getTotalCartProducts(cartId!).subscribe(response => {
      this.countProduct = response;
      console.log(response);
    },
    err => {
      console.log(err);
    }
    )
  }

}
