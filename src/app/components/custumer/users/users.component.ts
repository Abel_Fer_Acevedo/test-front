import { Component, OnInit } from '@angular/core';
import { UserResponse } from 'src/app/model/response/user-response';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit{
  countProduct = 0;
  userLogued: string | null = '';
  userIsVip: string | null = '';
  users: UserResponse[]=[];
  constructor(private userService: UserService, private local: LocalAuthService, private cartService: CartService){}

  ngOnInit(): void {
    this.getAllVIPUsers();
    this.userLogued = this.local.getName();
    this.getTotalCartProducts();
  }

  getAllVIPUsers(){
    this.userService.getAllVIPUsers().subscribe(
      response => {
        console.log(response);
        this.users = response;
      },
      err => {
        console.log(err);
      }
    )
  }

  getTotalCartProducts(){
    const cartId = this.local.getCartId();
    this.cartService.getTotalCartProducts(cartId!).subscribe(response => {
      this.countProduct = response;
      console.log(response);
    },
    err => {
      console.log(err);
    }
    )
  }

}
