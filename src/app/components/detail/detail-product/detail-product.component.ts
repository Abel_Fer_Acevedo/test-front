import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ProductResponse } from 'src/app/model/response/product-response';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';
import { ProductService } from 'src/app/services/product.service';


@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.css']
})
export class DetailProductComponent implements OnInit {
  countProduct = 0;
  userLogued: string | null = '';
  userIsVip: string | null = '';
  product!: ProductResponse;

  constructor(private route: ActivatedRoute,
    private productService: ProductService,
    private local: LocalAuthService,
    private cartService: CartService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const productId = +params['id']; // Obtener el ID de los parámetros de la ruta
      this.getProductDetails(productId);
    });
    this.userLogued = this.local.getName();
    this.userIsVip = this.local.getIsVip();
    this.getTotalCartProducts();

  }

  getProductDetails(productId: number) {
    this.productService.getProductById(productId).subscribe(
      response => {
        console.log("Producto:", response);
        this.product = response;
      },
      err => {
        console.log(err);
      }
    )
  }

  createNewTrolleyForUser() {
    const userId = this.local.getUserId();
    if (userId !== null && userId !== undefined) {
      this.cartService.createNewTrolleyForUser(userId).subscribe(
        response => {

          if (response && response.id) {
            this.local.setCartId(response.id);
          }
          console.log("Carrito creado en detalle del producto.")
        }, err => {
          console.log(err);
        }
      )
    }
  }

  addProductToCart() {
    const cartId = this.local.getCartId();
    const productId = this.product.id;
    if (cartId != null && productId != null) {
      this.cartService.addProduct(cartId, productId).subscribe(
        response => {
          console.log("Producto guardado:", response);
          const timeOutDuration = 1000;
          this.toastr.success(response.message, '', {
            timeOut: timeOutDuration,
          });

        },
        err => {
          console.log(err);

        }
      )
    }

  }

  getTotalCartProducts() {
    const cartId = this.local.getCartId();
    this.cartService.getTotalCartProducts(cartId!).subscribe(response => {
      this.countProduct = response;
      console.log(response);
    },
      err => {
        console.log(err);
      }
    )
  }


}
