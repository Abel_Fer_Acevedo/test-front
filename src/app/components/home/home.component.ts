import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductResponse } from 'src/app/model/response/product-response';
import { SpecialDateResponse } from 'src/app/model/response/special-date-response';
import { CartService } from 'src/app/services/cart.service';
import { LocalAuthService } from 'src/app/services/local-auth.service';
import { ProductService } from 'src/app/services/product.service';
import { SharedDataService } from 'src/app/services/shared-data.service';
import { SpecialDateService } from 'src/app/services/special-date.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  countProduct: number = 0;
  dates: SpecialDateResponse[] = [];
  messageStatus = '';
  userLogued: string | null = '';
  userIsVip: string | null = '';
  products: ProductResponse[] = [];

  constructor(private local: LocalAuthService,
              private productService: ProductService,
              private cartService: CartService,
              private router: Router,
              private userService: UserService,
              private toastr: ToastrService,
              private specialDateService: SpecialDateService) { }

  ngOnInit(): void {
    this.userLogued = this.local.getName();
    this.userIsVip = this.local.getIsVip();
    // const cartId = this.local.getCartId();
    //Cuando llega al home despues de loguearse se crea el cart
    this.createNewTrolleyForUser();
    setTimeout(() => {
      this.updateVipStatus();
    }, 1000)

    this.getAllProducts();
    this.specialDates();
    this.getTotalCartProducts();

  }

  getAllProducts() {
    this.productService.getAllProducts().subscribe(
      response => {
        console.log("Productos:", response);
        this.products = response;
      },
      err => {
        console.log(err);
      }
    )
  }

  createNewTrolleyForUser() {
    const userId = this.local.getUserId();
    if (userId !== null && userId !== undefined) {
      this.cartService.createNewTrolleyForUser(userId).subscribe(
        response => {
          console.log("Carrito: ", response);
          if (response && response.id) {
            this.local.setCartId(response.id);
          }

        }, err => {
          console.log(err);
        }
      )
    }
  }

  SignOff() {
    const cartId = this.local.getCartId();
    console.log("Cerrando session");
    this.cartService.deleteTrolley(cartId!).subscribe(
      response => {
        console.log("Eliminando carrito para cerrar sesion");
      },
      err => {
        console.log(err);
      }
    )
    setTimeout(() => {
      this.local.SignOff();
      this.router.navigateByUrl('');
    }, 1000)

  }

  updateVipStatus() {
    const userId = this.local.getUserId();
    if (userId != null) {
      this.userService.updateVipStatus(userId).subscribe(
        response => {
          this.messageStatus = response.message;
          console.log(response);
          if (response.message === 'Felicidades Ahora eres un cliente VIP"') {

            this.local.setIsVip('true');
            console.log("Se entro a alta de Vip")
            this.userIsVip = 'USUARIO VIP';
            const timeOutDuration = 4000;
            this.toastr.info(response.message, '', {
              timeOut: timeOutDuration,
            });
          }
          if (response.message === 'Lamentamos comunicarte que ya no eres cliente VIP') {
            this.local.setIsVip('false');
            console.log("Se entro al if de baja vip..")
            this.userIsVip = 'USUARIO STANDAR'
            const timeOutDuration = 4000;
            this.toastr.info(response.message, '', {
              timeOut: timeOutDuration,
            });
          } else {
            console.log("Continuar sin modificaciones");

          }

        },
        err => {
          console.log(err);
        }
      )
    }
  }

  specialDates(){
    this.specialDateService.getAllSpecialDates().subscribe(
      response => {
        console.log("Dates", response);
        this.dates = response;
      },
      err => {
        console.log(err);
      }
    )
  }

  getTotalCartProducts(){
    const cartId = this.local.getCartId();
    this.cartService.getTotalCartProducts(cartId!).subscribe(response => {
      this.countProduct = response;
      console.log(response);
    },
    err => {
      console.log(err);
    }
    )
  }



}
