export interface LoginResponse {

  userId: number;
  name: string;
  username: string;
  isVip: string;
  role: string;
  token: string;
  message: string;

}
