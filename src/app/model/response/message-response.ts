export interface MessageResponse {

  httpStatus: number;
  message: string;

}
