export interface ProductResponse {

  id: number;
  name: string;
  price: number;
  creationDate: string;
  amount: number;
  stock: boolean;
  urlImage: string;

}
