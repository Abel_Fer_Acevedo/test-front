export interface SpecialDateResponse {

  id: number;
  date: Date;

}
