import { ProductResponse } from "./product-response";

export interface TrolleyResponse {

  id: number;
  total: number;
  creationDate: string;
  purchaseDate: string | null;
  active: boolean;
  cartType: string;
  productResponseList: ProductResponse[];
}
