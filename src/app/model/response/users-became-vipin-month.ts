export interface UsersBecameVIPInMonth {

  id: number;
  name: string;
  username: string;
  password: string;
  lastPurchaseDate: Date;
  vip: boolean;
  registrationVipDate: Date;

}
