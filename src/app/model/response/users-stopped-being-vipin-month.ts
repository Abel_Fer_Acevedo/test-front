export interface UsersStoppedBeingVIPInMonth {


  id: number;
  name: string;
  username: string;
  password: string;
  lastPurchaseDate: Date;
  vip: boolean;
  vipCancellationDate: Date;

}
