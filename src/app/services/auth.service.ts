import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginRequest } from '../model/request/login-request';
import { Observable } from 'rxjs';
import { LoginResponse } from '../model/response/login-response';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private urlAuth = 'http://localhost:8080/auth';

  constructor(private http: HttpClient) { }

  public login(request: LoginRequest): Observable<LoginResponse>{
    const url = `${this.urlAuth}/login`;
    return this.http.post<LoginResponse>(url, request);
  }
}
