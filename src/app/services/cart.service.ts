import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TrolleyResponse } from '../model/response/trolley-response';
import { MessageResponse } from '../model/response/message-response';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  private urlCart = 'http://localhost:8080/carts';

  constructor(private http: HttpClient) {}

  public createNewTrolleyForUser(userId: number): Observable<TrolleyResponse>{
    const url = `${this.urlCart}/${userId}`;
    return this.http.post<TrolleyResponse>(url, null);
  }
  public addProduct(cartId:number, productId:number): Observable<MessageResponse>{
    const url = `${this.urlCart}/${cartId}/product/${productId}`;
    return this.http.put<MessageResponse>(url, null);
  }
  public calculateCartValue(cartId: number, cartType: string): Observable<number>{
    const url = `${this.urlCart}/calculateValue/${cartId}`;
    const params = new HttpParams().set('cartType', cartType);
    return this.http.get<number>(url, {params} );
  }
  public buyCart(userId: number): Observable<MessageResponse>{
    const url = `${this.urlCart}/buy/${userId}`;
    return this.http.post<MessageResponse>(url, null);
  }
  public deleteTrolley(cartId: number): Observable<string>{
    const url = `${this.urlCart}/${cartId}`;
    return this.http.delete<string>(url);
  }
  public removeProductFromCart(cartId: number, productId: number): Observable<MessageResponse>{
    const url = `${this.urlCart}/${cartId}/products/${productId}`;
    return this.http.delete<MessageResponse>(url);
  }
  public getTotalCartProducts(cartId: number): Observable<number>{
    const url = `${this.urlCart}/total/${cartId}`;
    return this.http.get<number>(url);
  }


}
