import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LocalAuthService {

  private readonly TOKEN_LOGIN = 'AuthToken';
  private readonly CART_TYPE = 'CartTipe';
  private readonly CART_ID = 'Cart_id';
  private readonly USER_ID = 'UserId';
  private readonly NAME_USER = 'NameUSER'
  private readonly IS_VIP = 'IsVip';

  constructor(private router: Router, private authService: AuthService) { }

  public setType(type: string): void {
    localStorage.setItem(this.CART_TYPE, type);
  }
  public getType(): string | null {
    return localStorage.getItem(this.CART_TYPE);
  }
  public setToken(token: string): void {
    localStorage.setItem(this.TOKEN_LOGIN, token);
  }
  public getToken(): string | null {
    return localStorage.getItem(this.TOKEN_LOGIN);
  }
  public setName(name: string): void {
    localStorage.setItem(this.NAME_USER, name);
  }
  public getName(): string | null {
    return localStorage.getItem(this.NAME_USER);
  }
  public logOut(): void {
    localStorage.removeItem(this.TOKEN_LOGIN);
  }
  public isLoggued(): boolean {
    return this.getToken() !== null;
  }
  public setIsVip(isVip: string): void {
    // Convert boolean to string before storing in localStorage
    localStorage.setItem(this.IS_VIP, isVip);
  }
  public getIsVip(): string | null {
    // Retrieve the string from localStorage
    const storedValue = localStorage.getItem(this.IS_VIP);
    return storedValue;
  }
  public setUserId(userId: number): void {
    localStorage.setItem(this.USER_ID, userId.toString());
  }
  public getUserId(): number | null {
    const userIdString = localStorage.getItem(this.USER_ID);
    return userIdString ? parseInt(userIdString, 10) : null;
  }
  public setCartId(cartId: number): void {
    localStorage.setItem(this.CART_ID, cartId.toString());
  }
  public getCartId(): number | null {
    const cartIdString = localStorage.getItem(this.CART_ID);
    return cartIdString ? parseInt(cartIdString, 10) : null;
  }
  public removeCart(): void {
    localStorage.removeItem(this.CART_ID);
  }

  SignOff(): void{
    localStorage.removeItem(this.TOKEN_LOGIN);
    localStorage.removeItem(this.CART_ID);
    localStorage.removeItem(this.USER_ID);
    localStorage.removeItem(this.NAME_USER);
    localStorage.removeItem(this.IS_VIP);
    localStorage.removeItem(this.CART_TYPE);
  }



}
