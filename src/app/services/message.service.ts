import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private messageSubject = new Subject<string>();
  constructor() { }

  sendMessage(message: string): void {
    this.messageSubject.next(message);
  }

  getMessage(): Subject<string> {
    return this.messageSubject;
  }
}
