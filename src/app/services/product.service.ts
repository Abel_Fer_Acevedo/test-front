import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductResponse } from '../model/response/product-response';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private urlProduct = 'http://localhost:8080/products';

  constructor(private http: HttpClient) { }

  public getAllProducts(): Observable<ProductResponse[]>{
    const url = `${this.urlProduct}`;
    return this.http.get<ProductResponse[]>(this.urlProduct);
  }
  public getProductById(id: number): Observable<ProductResponse>{
    const url = `${this.urlProduct}/${id}`;
    return this.http.get<ProductResponse>(url);
  }
}
