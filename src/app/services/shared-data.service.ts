import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService {
  private productCountSubject = new BehaviorSubject<number>(0);
  productCount$ = this.productCountSubject.asObservable();

  updateProductCount(newCount: number): void {
    console.log("Servicio compartido", newCount)
    this.productCountSubject.next(newCount);
  }
  getProductCount(): Observable<number> {
    return this.productCount$;
  }
}
