import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SpecialDateResponse } from '../model/response/special-date-response';

@Injectable({
  providedIn: 'root'
})
export class SpecialDateService {

  private urlDates = 'http://localhost:8080/dates';

  constructor(private http: HttpClient) { }

  public getAllSpecialDates(): Observable<SpecialDateResponse[]>{
    const url = `${this.urlDates}`;
    return this.http.get<SpecialDateResponse[]>(url);
  }
}
