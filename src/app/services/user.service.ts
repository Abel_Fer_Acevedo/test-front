import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { MessageResponse } from '../model/response/message-response';
import { MessageService } from './message.service';
import { UserResponse } from '../model/response/user-response';
import { UsersBecameVIPInMonth } from '../model/response/users-became-vipin-month';
import { UsersStoppedBeingVIPInMonth } from '../model/response/users-stopped-being-vipin-month';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlUser = 'http://localhost:8080/users';

  constructor(private http: HttpClient, private messageService: MessageService) { }

  public updateVipStatus(userId: number): Observable<MessageResponse> {
    const url = `${this.urlUser}/${userId}`;
    return this.http.put<MessageResponse>(url, {}).pipe(
      tap((response) => {
        this.messageService.sendMessage(response.message);
      })
    );
  }

  updateVipStatusParams(userId: number, currentDate?: string): Observable<any> {

    let params = new HttpParams();
    if (currentDate) {
      params = params.set('currentDate', currentDate);
    }
    return this.http.put<any>(`${this.urlUser}/params/${userId}`, null, { params });
  }

  getAllVIPUsers(): Observable<UserResponse[]> {
    const url = `${this.urlUser}/vip`;
    return this.http.get<UserResponse[]>(url);
  }

  getUsersBecameVIPInMonth(month: number): Observable<UsersBecameVIPInMonth[]>{
    const url = `${this.urlUser}/became-vip/${month}`;
    return this.http.get<UsersBecameVIPInMonth[]>(url);
  }

  getUsersStoppedBeingVIPInMonth(month: number): Observable<UsersStoppedBeingVIPInMonth[]>{
    const url = `${this.urlUser}/stopped-being-vip/${month}`;
    return this.http.get<UsersStoppedBeingVIPInMonth[]>(url);
  }

}
